const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/example');

const Room = require('./models/Room')
const Building = require('./models/Building');

async function clearDB () {
    await Room.deleteMany({})
    await Building.deleteMany({})
}

async function main() {
    await clearDB()
    const informaticsBuilding = new Building({ name: 'Informatics', floor:11 })
    const newInformaticsBuilding = new Building({ name: 'New Informatics', floor:20 })
    const room3C01 = new Room ({ name: '3C01', capacity: 200, floor: 3, building: informaticsBuilding })
    informaticsBuilding.rooms.push(room3C01)
    const room4C01 = new Room ({ name: '4C01', capacity: 200, floor: 4, building: informaticsBuilding })
    informaticsBuilding.rooms.push(room4C01)
    const room5C01 = new Room ({ name: '5C01', capacity: 200, floor: 5, building: informaticsBuilding })
    informaticsBuilding.rooms.push(room5C01)
    await informaticsBuilding.save()
    await newInformaticsBuilding.save()
    await room3C01.save()
    await room4C01.save()
    await room5C01.save()
}

main().then(function () {
    console.log('Finish')
})