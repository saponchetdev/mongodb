const mongoose = require('mongoose');
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example');

async function main () {
    // Update
    // const room = await Room.findById('625eb7d0f62ace604ac0e5dc')
    // room.capacity = 150
    // room.save()
    // console.log(room)

    const room = await Room.findOne({ capacity: { $gte: 100}}).populate('building')  
    console.log(room)
    console.log('--------------------------------')
    const rooms = await Room.find({ capacity: { $gte: 100}}).populate('building')    
    console.log(rooms)
    const building = await Building.find({}).populate('rooms')
    console.log(JSON.stringify(building))
}
 
main().then(() => {
    console.log('Finish')
})